FROM openjdk:8-jre
COPY target /springcloudconfigapp
WORKDIR /springcloudconfigapp
ENTRYPOINT ["java", "-jar", "springconfig-0.0.1-SNAPSHOT.jar"]